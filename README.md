# next2go

> author: Guangsen(Sam) Nie

## Environment setup

### Prerequisite setup

> - node (v13.14.0)
>   - (optional) nvm (https://github.com/nvm-sh/nvm), `nvm install && nvm use`
>   - https://nodejs.org/en/

### Install vue cli

```
npm install -g @vue/cli @vue/cli-service-global
```

## Project setup

```
npm install
```

### Compiles and hot-reloads for development

```
npm run serve
```

### Compiles and minifies for production

```
npm run build
```

### Lints and fixes files

```
npm run lint
```
