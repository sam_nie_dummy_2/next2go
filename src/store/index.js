import Vuex from 'vuex';

// example purpose, should set elsewhere.
//haven't implement a size detector to dynamically reload if the one catory having races less then five.

const url = 'https://api.neds.com.au/rest/v1/racing/?method=nextraces&count=45';
const header = { Accept: 'application/json' };
const categoryName = {
	'4a2788f8-e825-4d36-9894-efd4baf1cfae': 'horse',
	'9daef0d7-bf3c-4f50-921d-8e818c60fe61': 'greyhound',
	'161d9be2-e909-4326-8c2c-35ed71fb460b': 'harness',
};
/**
 * build list function, dump unused information and provide correct order list
 * @param {*} originList  original list from api call
 * @returns {} object contains order list (for time sorting), three categories list.
 */
const buildStage = (originList) => {
	let races = {
		allRaces: [],
		timeOrderList: [],
	};
	//get correct time order list as next_to_go_ids not necessary provide correct order
	let sortList = [];
	for (let item in originList.data.race_summaries) {
		sortList.push([
			item,
			originList.data.race_summaries[item].advertised_start.seconds,
		]);
	}
	races.timeOrderList = sortList
		.sort((a, b) => {
			return a[1] - b[1];
		})
		.map((e) => e[0]);
	//console.log(sortList);
	races.timeOrderList.map((key) => {
		const race = originList.data.race_summaries[key];
		const singleRace = {
			raceId: race.race_id,
			meetingName: race.meeting_name,
			raceNumber: race.race_number,
			advertisedStart: race.advertised_start.seconds,
			raceCategory: categoryName[race.category_id],
		};

		races.allRaces.push(singleRace);

		return;
	});

	return races;
};

export default new Vuex.Store({
	state: {
		mainStage: {
			allRaces: [],
			timeOrderList: [],
		},
		displayList: [],
		activeFilters: ['horse', 'greyhound', 'harness'],
	},
	mutations: {
		updateStage(state, payload) {
			state.mainStage = payload;
		},
		updateFilter(state, payload) {
			state.activeFilters = payload;
		},
		updateList(state) {
			const list = state.mainStage;
			const filter = state.activeFilters;
			let raceList = [];
			list.timeOrderList.forEach((element) => {
				list.allRaces.map((e) => {
					if (e.raceId == element && filter.includes(e.raceCategory)) {
						if (raceList.length < 5) {
							raceList.push(e);
						}
						return e;
					}
				});
			});

			state.displayList = raceList;
		},
		popsList(state, raceId) {
			const list = state.mainStage;
			const filter = state.activeFilters;
			let raceList = [];
			list.timeOrderList.forEach((element) => {
				list.allRaces.map((e) => {
					if (
						e.raceId == element &&
						filter.includes(e.raceCategory) &&
						e.raceId !== raceId
					) {
						if (raceList.length < 5) {
							raceList.push(e);
						}
						return e;
					}
				});
			});
			state.displayList = raceList;
		},
	},
	actions: {
		async getNextList(state) {
			const nextList = await fetch(url, header);
			//todo: error handling
			const stageList =
				nextList.status === 200 ? await buildStage(await nextList.json()) : [];

			state.commit('updateStage', stageList);
			console.log(stageList);
			state.commit('updateList');
		},
	},
	getters: {
		getMainStage: (state) => state.mainStage,
		getFilters: (state) => state.activeFilters,
		getDisplayList: (state) => state.displayList,
	},
});
